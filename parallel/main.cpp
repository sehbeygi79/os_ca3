#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <math.h>
#include <iomanip>
#include <pthread.h>
// #include <chrono>

using namespace std;

#define DATASET_NAME "dataset_"
#define DATASET_FORMAT ".csv"
#define EXPENSIVE 1
#define CHEAP 0
#define COLUMN_COUNT 9
#define COL0 "LotArea"
#define COL1 "OverallQual"
#define COL2 "OverallCond"
#define COL3 "YearBuilt"
#define COL4 "TotalBsmtSF"
#define COL5 "GrLivArea"
#define COL6 "GarageCars"
#define COL7 "GarageArea"
#define COL8 "SalePrice"

enum columns {LotArea, OverallQual, OverallCond,
              YearBuilt, TotalBsmtSF, GrLivArea,
              GarageCars, GarageArea, SalePrice,
              GuessPrice};

struct extractFileArg {
    int turn;
    string address;
};

struct labelizeArg {
    int start;
    int end;
    int priceThreshold;
};

struct info {
    float cheap;
    float expensive;
    int column;
};

int turn;
vector<vector<int> > dataset;

pthread_mutex_t lock;
vector<info> means;
vector<info> stds;
/////////////////////////////////////////////////////////////////////////////////////////////////
int getFileCount(string path) {
    for (int i=0; 1; i++) {
        string name = path + DATASET_NAME + to_string(i) + DATASET_FORMAT;

        fstream stream(name);
        if (!stream)
            return i;
    }
}

vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter)) {
        if (tok != "")
            internal.push_back(tok);
    }

    return internal;
}

vector<int> splitInt(string str, char delimiter) {
    vector<int> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter))
    {
        if (tok != "")
            internal.push_back(stoi(tok));
    }

    return internal;
}

int labelToIndex(vector<string> labels, string label) {
    for (int i = 0; i < labels.size(); i++) {
        if (labels[i].substr(0, label.length()) == label)
            return i;   
    }
    return -1;
}

void* extractFile_sr(void* arg) {
    struct extractFileArg *a = (struct extractFileArg *)arg;

    fstream stream(a->address);
    if(!stream)
        return {};

    string line;
    vector<vector<int> > outputFile;

    int columnOrder[COLUMN_COUNT];
    for (int i=0; getline(stream, line); i++) {
        if (i == 0) {
            vector<string> labels = split(line, ',');

            columnOrder[0] = labelToIndex(labels, COL0);
            columnOrder[1] = labelToIndex(labels, COL1);
            columnOrder[2] = labelToIndex(labels, COL2);
            columnOrder[3] = labelToIndex(labels, COL3);
            columnOrder[4] = labelToIndex(labels, COL4);
            columnOrder[5] = labelToIndex(labels, COL5);
            columnOrder[6] = labelToIndex(labels, COL6);
            columnOrder[7] = labelToIndex(labels, COL7);
            columnOrder[8] = labelToIndex(labels, COL8);
        }
        else {
            vector<int> tokenizedLine = splitInt(line, ',');
            vector<int> holder;
            for (int j=0; j<COLUMN_COUNT; j++) {
                holder.push_back(tokenizedLine[columnOrder[j]]);
            }

            outputFile.push_back(holder);
        }
    }

    while (a->turn != turn) {}
    pthread_mutex_lock(&lock);
    dataset.insert(dataset.end(), outputFile.begin(), outputFile.end());
    turn++;
    pthread_mutex_unlock(&lock);

    pthread_exit(NULL);
}

// gets root folder of .csv files and count of the files
void extractFile_par(string datasetPath, int threadCount) {
    pthread_t threads[threadCount];

    struct extractFileArg args[threadCount];
    for (int i=0; i< threadCount; i++) {
        args[i].address = datasetPath + DATASET_NAME + to_string(i) + DATASET_FORMAT;
        args[i].turn = i;
    }

    turn = 0;
    if (pthread_mutex_init(&lock, NULL) != 0) {
        printf("mutex init has failed\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < threadCount; i++) {
        pthread_create(threads + i, NULL, &extractFile_sr, (void *)(args + i));
    }

    for (int i=0; i< threadCount; i++) {
        pthread_join(*(threads+i), NULL);
    }

    pthread_mutex_destroy(&lock);
}

void* labelize_sr(void* arg) {
    struct labelizeArg *a = (struct labelizeArg *)arg;

    for (int i = a->start; i < a->end + 1; i++) {
        if (dataset[i][SalePrice] < a->priceThreshold)
            dataset[i][SalePrice] = CHEAP;
        else
            dataset[i][SalePrice] = EXPENSIVE;
    }
    pthread_exit(NULL);
}

void labelize_par(int threadCount, int priceThreshold) {
    pthread_t threads[threadCount];

    int length = dataset.size() / threadCount;
    struct labelizeArg args[threadCount];
    for (int i = 0; i < threadCount; i++) {
        args[i].priceThreshold = priceThreshold;
        args[i].start = (i * length);
        if (i == threadCount - 1)
            args[i].end = dataset.size() - 1;
        else
            args[i].end = args[i].start + length - 1;
    }

    for (int i = 0; i < threadCount; i++) {
        pthread_create(threads + i, NULL, &labelize_sr, (void *)(args + i));
    }

    for (int i = 0; i < threadCount; i++) {
        pthread_join(*(threads+i), NULL);
    }
}

void labelize(int priceThreshold) {
    for (int i=0; i<dataset.size(); i++) {
        if (dataset[i][SalePrice] < priceThreshold)
            dataset[i][SalePrice] = CHEAP;
        else
            dataset[i][SalePrice] = EXPENSIVE;
    }
}

void* calcMean_sr(void* arg) {
    int* column = (int*) arg;

    struct info *m = (struct info *)malloc(sizeof(struct info));

    float sum_cheap = 0.0;
    int count_cheap = 0;
    float sum_exp = 0.0;
    int count_exp = 0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == 0) {
            sum_cheap += dataset[i][*column];
            count_cheap++;
        }
        else {
            sum_exp += dataset[i][*column];
            count_exp++;
        }
    }
    m->cheap = sum_cheap / count_cheap;
    m->expensive = sum_exp / count_exp;
    m->column = *column;

    pthread_mutex_lock(&lock);
    means.push_back(*m);
    pthread_mutex_unlock(&lock);

    pthread_exit(NULL);
}

void calcMeans_par() {
    pthread_t threads[GarageArea + 1];

    int columns[GarageArea + 1];
    for (int i = 0; i < GarageArea + 1; i++) {
        columns[i] = i;
    }

    if (pthread_mutex_init(&lock, NULL) != 0) {
        printf("mutex init has failed\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < GarageArea + 1; i++) {
        pthread_create(threads + i, NULL, &calcMean_sr, (void *)(columns + i));
    }

    for (int i=0; i< GarageArea + 1; i++) {
        pthread_join(*(threads+i), NULL);
    }

    pthread_mutex_destroy(&lock);

}

void* calcSTD_sr(void* arg) {
    int* column = (int*) arg;

    struct info m;
    for (int i = 0; i < means.size(); i++) {
        if (means[i].column == *column)
            m = means[i];
    }

    struct info *s = (struct info *)malloc(sizeof(struct info));

    float sum_cheap = 0.0;
    int count_cheap = 0;
    float sum_exp = 0.0;
    int count_exp = 0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == 0) {
            sum_cheap += (dataset[i][*column] - m.cheap) * (dataset[i][*column] - m.cheap);
            count_cheap++;
        }
        else {
            sum_exp += (dataset[i][*column] - m.expensive) * (dataset[i][*column] - m.expensive);
            count_exp++;
        }
    }

    s->cheap = sqrt(sum_cheap / count_cheap);
    s->expensive = sqrt(sum_exp / count_exp);
    s->column = *column;

    pthread_mutex_lock(&lock);
    stds.push_back(*s);
    pthread_mutex_unlock(&lock);

    pthread_exit(NULL);
}

void calcSTDs_par() {
    pthread_t threads[GarageArea + 1];

    int columns[GarageArea + 1];
    for (int i = 0; i < GarageArea + 1; i++) {
        columns[i] = i;
    }

    if (pthread_mutex_init(&lock, NULL) != 0) {
        printf("mutex init has failed\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < GarageArea + 1; i++) {
        pthread_create(threads + i, NULL, &calcSTD_sr, (void *)(columns + i));
    }

    for (int i=0; i< GarageArea + 1; i++) {
        pthread_join(*(threads+i), NULL);
    }

    pthread_mutex_destroy(&lock);
}

void guessPriceClass(float meanExp, float stdExp) {

    for (int i=0; i< dataset.size(); i++) {
        if ((dataset[i][GrLivArea] >= meanExp - stdExp) && (dataset[i][GrLivArea] <= meanExp + stdExp))
            dataset[i].push_back(EXPENSIVE);
        else
            dataset[i].push_back(CHEAP);
    }
}

struct info getColumnInfo(vector<info> v, int column) {
    for (int i=0; i<v.size(); i++) {
        if (v[i].column == column)
            return v[i];
    }
    struct info tmp;
    return tmp;
}

float calcAccuracy() {
    float hitCount = 0.0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == dataset[i][GuessPrice])
            hitCount++;
    }
    return (hitCount / dataset.size()) * 100;
}


int main(int argc, char* argv[]) {
    // auto start = chrono::high_resolution_clock::now();

    if (argc != 3) {
        cout << "bad arguments" << endl;
        exit(EXIT_FAILURE);
    }
    string datasetPath(argv[1]);
    int priceThreshold = atoi(argv[2]);
    int fileCount = getFileCount(datasetPath);

    extractFile_par(datasetPath, fileCount);
    // labelize_par(5, priceThreshold);
    labelize(priceThreshold);

    calcMeans_par();
    calcSTDs_par();

    guessPriceClass(getColumnInfo(means, GrLivArea).expensive, getColumnInfo(stds, GrLivArea).expensive);
    cout << "Accuracy: " << fixed << setprecision(2) << calcAccuracy() << '%';
    
    // auto end = chrono::high_resolution_clock::now();
    // auto duration = chrono::duration_cast<chrono::microseconds>(end - start);
    // cout << "\nduration: " << duration.count() / 1000 << "ms" << endl;

    exit(EXIT_SUCCESS);
}