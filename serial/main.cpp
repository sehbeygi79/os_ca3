#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <math.h>
#include <iomanip>
// #include <chrono>

using namespace std;

#define DATASET_NAME "dataset.csv"
#define EXPENSIVE 1
#define CHEAP 0
#define COLUMN_COUNT 9
#define COL0 "LotArea"
#define COL1 "OverallQual"
#define COL2 "OverallCond"
#define COL3 "YearBuilt"
#define COL4 "TotalBsmtSF"
#define COL5 "GrLivArea"
#define COL6 "GarageCars"
#define COL7 "GarageArea"
#define COL8 "SalePrice"

enum columns {LotArea, OverallQual, OverallCond,
              YearBuilt, TotalBsmtSF, GrLivArea,
              GarageCars, GarageArea, SalePrice,
              GuessPrice};

vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter)) {
        if (tok != "")
            internal.push_back(tok);
    }

    return internal;
}

vector<int> splitInt(string str, char delimiter) {
    vector<int> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter))
    {
        if (tok != "")
            internal.push_back(stoi(tok));
    }

    return internal;
}

int labelToIndex(vector<string> labels, string label) {
    for (int i = 0; i < labels.size(); i++) {
        if (labels[i].substr(0, label.length()) == label)
            return i;   
    }
    return -1;
}

vector<vector<int> > extractFile(string path) {

    fstream stream(path);
    if(!stream)
        return {};

    string line;
    vector<vector<int> > outputFile;

    int columnOrder[COLUMN_COUNT];
    for (int i=0; getline(stream, line); i++) {
        if (i == 0) {
            vector<string> labels = split(line, ',');

            columnOrder[0] = labelToIndex(labels, COL0);
            columnOrder[1] = labelToIndex(labels, COL1);
            columnOrder[2] = labelToIndex(labels, COL2);
            columnOrder[3] = labelToIndex(labels, COL3);
            columnOrder[4] = labelToIndex(labels, COL4);
            columnOrder[5] = labelToIndex(labels, COL5);
            columnOrder[6] = labelToIndex(labels, COL6);
            columnOrder[7] = labelToIndex(labels, COL7);
            columnOrder[8] = labelToIndex(labels, COL8);
        }
        else {
            vector<int> tokenizedLine = splitInt(line, ',');
            vector<int> holder;
            for (int j=0; j<COLUMN_COUNT; j++) {
                holder.push_back(tokenizedLine[columnOrder[j]]);
            }

            outputFile.push_back(holder);
        }
    }

    return outputFile;
}

void labelize(vector<vector<int> > &dataset, int priceThreshold) {
    for (int i=0; i<dataset.size(); i++) {
        if (dataset[i][SalePrice] < priceThreshold)
            dataset[i][SalePrice] = CHEAP;
        else
            dataset[i][SalePrice] = EXPENSIVE;
    }
}

vector<float> calcMean(vector<vector<int> > dataset, int column) {
    vector<float> mean_ch_exp;

    float sum_cheap = 0.0;
    int count_cheap = 0;
    float sum_exp = 0.0;
    int count_exp = 0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == 0) {
            sum_cheap += dataset[i][column];
            count_cheap++;
        }
        else {
            sum_exp += dataset[i][column];
            count_exp++;
        }
    }
    mean_ch_exp.push_back(sum_cheap / count_cheap);
    mean_ch_exp.push_back(sum_exp / count_exp);

    return mean_ch_exp;
}

vector<float> calcSTD(vector<vector<int> > dataset, int column, vector<float> means) {
    vector<float> std_ch_exp;

    float sum_cheap = 0.0;
    int count_cheap = 0;
    float sum_exp = 0.0;
    int count_exp = 0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == 0) {
            sum_cheap += (dataset[i][column] - means[0]) * (dataset[i][column] - means[0]);
            count_cheap++;
        }
        else {
            sum_exp += (dataset[i][column] - means[1]) * (dataset[i][column] - means[1]);
            count_exp++;
        }
    }
    std_ch_exp.push_back(sqrt(sum_cheap / count_cheap));
    std_ch_exp.push_back(sqrt(sum_exp / count_exp));

    return std_ch_exp;
}

void guessPriceClass(vector<vector<int> > &dataset, float meanExp, float stdExp) {

    for (int i=0; i< dataset.size(); i++) {
        if ((dataset[i][GrLivArea] >= meanExp - stdExp) && (dataset[i][GrLivArea] <= meanExp + stdExp))
            dataset[i].push_back(EXPENSIVE);
        else
            dataset[i].push_back(CHEAP);
    }
}

float calcAccuracy(vector<vector<int> > dataset) {
    float hitCount = 0.0;
    for (int i = 0; i < dataset.size(); i++) {
        if (dataset[i][SalePrice] == dataset[i][GuessPrice])
            hitCount++;
    }
    return (hitCount / dataset.size()) * 100;
}


int main(int argc, char* argv[]) {
    // auto start = chrono::high_resolution_clock::now();

    if (argc != 3) {
        cout << "bad arguments" << endl;
        exit(EXIT_FAILURE);
    }
    string datasetPath(argv[1]);
    int priceThreshold = atoi(argv[2]);
    vector<vector<int> > dataset = extractFile(datasetPath + DATASET_NAME);

    labelize(dataset, priceThreshold);

    vector<vector<float>> means(COLUMN_COUNT - 1, {0.0, 0.0});
    for (int i = 0; i < GarageArea + 1; i++) {
        means[i] = calcMean(dataset, i);
    }

    vector<vector<float>> stds(COLUMN_COUNT - 1, {0.0, 0.0});
    for (int i = 0; i < GarageArea + 1; i++) {
        stds[i] = calcSTD(dataset, i, means[i]);
    }

    guessPriceClass(dataset, means[GrLivArea][1], stds[GrLivArea][1]);

    cout << "Accuracy: " << fixed << setprecision(2) << calcAccuracy(dataset) << '%';

    // auto end = chrono::high_resolution_clock::now();
    // auto duration = chrono::duration_cast<chrono::microseconds>(end - start);
    // cout << "\nduration: " << duration.count()/1000 << "ms" << endl;

    exit(EXIT_SUCCESS);
}